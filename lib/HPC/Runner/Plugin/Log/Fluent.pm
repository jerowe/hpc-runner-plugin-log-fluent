package HPC::Runner::Plugin::Log::Fluent;

use 5.008_005;
our $VERSION = '0.01';

use Moose::Role;
use Data::Dumper;
use Fluent::Logger;

use namespace::autoclean;

has 'fluent_logger' =>(
    is => 'rw',
    default => sub {
        my $self = shift;
        my $logger = Fluent::Logger->new(
            tag_prefix => 'analysis',
            host       => '10.230.9.182',
            port       => 24224,
        );

        return $logger;
    }
);

before 'log_cmd_messages' => sub {
    my $self = shift;
    my $level = shift;
    my $msg = shift;
    my $cmdpid = shift;

    my $href = {};
    if($msg =~ m/ExitCode/){
        my($tmp) = $msg =~ m/ExitCode (.*)$/;
        $href->{ExitCode} = $tmp;
    }
    my $tags = "";

    if($self->can('tags')){
        $tags = join(",", @{$self->tags});
    }
    else{
        $tags = $self->jobname;
    }
    $tags .= ",PID$cmdpid";

    $href->{msg} = $msg;
    $href->{tags} = $tags;

    #TODO Get projectname, batch, etc
    #Calculate total batches before submitting...
    $self->fluent_logger->post("project.jobname.batchnumber.counter", $href);
};

1;
__END__

=encoding utf-8

=head1 NAME

HPC::Runner::Plugin::Log::Fluent - Blah blah blah

=head1 SYNOPSIS

  use HPC::Runner::Plugin::Log::Fluent;

=head1 DESCRIPTION

HPC::Runner::Plugin::Log::Fluent is

=head1 AUTHOR

Jillian Rowe E<lt>jillian.e.rowe@gmail.comE<gt>

=head1 COPYRIGHT

Copyright 2016- Jillian Rowe

=head1 LICENSE

This library is free software; you can redistribute it and/or modify
it under the same terms as Perl itself.

=head1 SEE ALSO

=cut
