# NAME

HPC::Runner::Plugin::Log::Fluent - Blah blah blah

# SYNOPSIS

    use HPC::Runner::Plugin::Log::Fluent;

# DESCRIPTION

HPC::Runner::Plugin::Log::Fluent is

# AUTHOR

Jillian Rowe &lt;jillian.e.rowe@gmail.com>

# COPYRIGHT

Copyright 2016- Jillian Rowe

# LICENSE

This library is free software; you can redistribute it and/or modify
it under the same terms as Perl itself.

# SEE ALSO
